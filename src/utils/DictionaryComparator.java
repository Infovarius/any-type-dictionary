package utils;

import dictionary.IDictionary;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class DictionaryComparator implements Comparator<IDictionary> {
    private Collator spCollator = Collator.getInstance(new Locale("ru", "RU"));

    public int compare(IDictionary e1, IDictionary e2) {
        return spCollator.compare(e1.getLemma(), e2.getLemma());
    }
}
