package sparql;

import configuration.PathConfig;
import enums.PartOfSpeech;
import org.apache.jena.rdf.model.RDFNode;
import org.javatuples.Pair;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class QueryParser {
    private static List<Pair<String, String>> entitiesQ;

    public QueryParser() throws IOException {
        entitiesQ = new ArrayList<>();
        FileReader fileReader = new FileReader(PathConfig.getEntitiesNamesPath());
        Scanner scan = new Scanner(fileReader);
        while (scan.hasNext()) {
            String[] array = scan.nextLine().split("-");
            entitiesQ.add(new Pair<>(array[0], array[1]));
        }
    }

    static String ifNull(RDFNode node) {
        return node.toString();
    }

    static Pair<PartOfSpeech, String> convertQToSpeechPart(RDFNode node) {
        PartOfSpeech part0 = PartOfSpeech.valueOf(convertQToWord(node));
        String part1 = "";
        switch (part0) {
            case Noun: {
                part1 = "существительное";
                break;
            }
            case Verb: {
                part1 = "глагол";
                break;
            }
            case Adjective: {
                part1 = "прилагательное";
                break;
            }
            case Adverb: {
                part1 = "наречие";
                break;
            }
        }
        return new Pair<>(part0, part1);
    }

    static String convertQToWord(RDFNode node) {
        if (node == null)
            return "";
        String value = ifNull(node);
        String finalValue = value;
        Optional<Pair<String, String>> newPair = entitiesQ.stream()
                .filter(pair -> pair.getValue0().compareTo(finalValue) == 0).findAny();
        if (newPair.isPresent()) {
            value = newPair.get().getValue1();
        }
        return value;
    }

    private static List<String> unknowns = Arrays.asList(
            "http://www.wikidata.org/entity/Q52943434", "http://www.wikidata.org/entity/Q54020116",
            "http://www.wikidata.org/entity/Q52431970", "http://www.wikidata.org/entity/Q179230");

    public static boolean existUnknowns(RDFNode node) {
        if (node == null)
            return true;
        String value = ifNull(node);
        List<String> values = Arrays.asList(value.split(", "));
        return values.stream().anyMatch(value0 -> unknowns.contains(value0));
    }

    static List<String> convertFeaturesQToWord(RDFNode node) {
        if (node == null)
            return new ArrayList<>();
        String value = ifNull(node);
        List<String> values = Arrays.asList(value.split(", "));
        List<Pair<String, String>> newPair = entitiesQ.stream()
                .filter(pair -> values.contains(pair.getValue0())).collect(Collectors.toList());
        return newPair.stream().map(Pair::getValue1).collect(Collectors.toList());
    }

}
