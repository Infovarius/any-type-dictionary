package sparql;

import dictionary.IDictionary;
import dictionary.explanatory.ExplanatoryDictionary;
import dictionary.grammatical.GrammaticalDictionary;
import dictionary.grammatical.WordForms;
import enums.DictionaryType;
import enums.WordOrderType;
import frontend.controllers.MainViewController;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SparqlService {
    private static String wikidata = "https://query.wikidata.org/sparql";

    public static IDictionary getResponseForOne(IDictionary data, String query) {
        System.out.println(query);
        QueryExecution e = QueryExecutionFactory.sparqlService(wikidata, query);
        try {
            ResultSet rs = e.execSelect();
            switch (data.getDictionaryType()) {
                case Grammatical: {
                    List<WordForms> wordForms = new ArrayList<>();
                    if (!rs.hasNext()) {
                        MainViewController.addToForLog(
                                String.format("По запросу \"%s\" (грамматический словарь) ничего не найдено\n",
                                        data.getLemma()));
                        return null;
                    }
                    List<GrammaticalDictionary> dictionary = new ArrayList<>();
                    while (rs.hasNext()) {
                        QuerySolution solution = rs.next();
                        if (QueryParser.existUnknowns(solution.get("lem")))
                            continue;
                        dictionary.add(new GrammaticalDictionary(data.getLemma(),
                                QueryParser.convertQToSpeechPart(solution.get("myPart")),
                                QueryParser.convertQToWord(solution.get("gender")),
                                new ArrayList<>(Collections.singleton(
                                        new WordForms(QueryParser.ifNull(solution.get("wordLabel")),
                                                QueryParser.convertFeaturesQToWord(solution.get("lem")))))
                        ));
//                        ((GrammaticalDictionary) data).setPart(QueryParser.convertQToSpeechPart(solution.get("myPart")));
//                        ((GrammaticalDictionary) data).setGender(QueryParser.convertQToWord(solution.get("gender")));
//                        WordForms wordForm = new WordForms(QueryParser.ifNull(solution.get("wordLabel")),
//                                QueryParser.convertFeaturesQToWord(solution.get("lem")));
//                        wordForms.add(wordForm);
                    }
                    List<GrammaticalDictionary> empties = dictionary.stream()
                            .filter(word -> word.getGender().compareTo("") == 0).collect(Collectors.toList());
                    List<GrammaticalDictionary> notEmpties = dictionary.stream()
                            .filter(word -> word.getGender().compareTo("") != 0).collect(Collectors.toList());

                    if (empties.size() > 0 && notEmpties.size() > 0) {
                        wordForms.addAll(dictionary.stream()
                                .filter(word -> word.getGender().compareTo("") == 0)
                                .flatMap(word -> word.getWordForms().stream()).collect(Collectors.toList()));
                        ((GrammaticalDictionary) data).setPart(empties.get(0).getPart());
                        ((GrammaticalDictionary) data).setGender(empties.get(0).getGender());
                    } else {
                        wordForms.addAll(dictionary.stream()
                                .flatMap(word -> word.getWordForms().stream()).collect(Collectors.toList()));
                        ((GrammaticalDictionary) data).setPart(dictionary.get(0).getPart());
                        ((GrammaticalDictionary) data).setGender(dictionary.get(0).getGender());
                    }
                    ((GrammaticalDictionary) data).setWordForms(wordForms);

                    for (WordForms word : wordForms) {
                        System.out.print(word.getWord() + " ");
                        System.out.println(String.join(", ", word.getLemmas()));
                    }
                    break;
                }
                case Explanatory: {
                    List<String> senses = new ArrayList<>();
                    if (!rs.hasNext()) {
                        MainViewController.addToForLog(
                                String.format("По запросу \"%s\" (толковый словарь) ничего не найдено\n",
                                        data.getLemma()));
                        return null;
                    }
                    while (rs.hasNext()) {
                        QuerySolution solution = rs.next();
                        senses.add(QueryParser.ifNull(solution.get("senseLabel")));
                    }
                    ((ExplanatoryDictionary) data).setSenses(senses);
                    break;
                }
            }
            e.close();
            return data;
        } catch (Exception e1) {
//            e1.getStackTrace();
            MainViewController.addToForLog("Не получается выполнить запрос. Возможно нет подключения к интернету\n");
            return null;
        }
    }

    public static List<IDictionary> getResponseForAll(DictionaryType dictionaryType, WordOrderType wordOrderType, String query) {
        List<IDictionary> results = new ArrayList<>();
        System.out.println(query);
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(wikidata, query);
        ResultSet rs;
        try {
            rs = queryExecution.execSelect();
        } catch (Exception e) {
//            e1.getStackTrace();
            MainViewController.addToForLog("Не получается выполнить запрос. Возможно нет подключения к интернету\n");
            return null;
        }
        switch (dictionaryType) {
            case Grammatical: {
                while (rs.hasNext()) {
                    QuerySolution solution = rs.next();
                    results.add(new GrammaticalDictionary(QueryParser.ifNull(solution.get("lemmaLabel"))));
                    System.out.println(QueryParser.ifNull(solution.get("lemmaLabel")));
                }
                break;
            }
            case Explanatory: {
                while (rs.hasNext()) {
                    QuerySolution solution = rs.next();
                    results.add(new ExplanatoryDictionary(
                            QueryParser.ifNull(solution.get("lemmaLabel"))));
                    System.out.println(QueryParser.ifNull(solution.get("lemmaLabel")));
                }
                break;
            }
        }
        switch (wordOrderType) {
            case Lexicografical: {
                Utils.setLexicograficalOrder(results);
                break;
            }
            case Inverse: {
                Utils.setInverseOrder(results);
                break;
            }
        }

//            ResultSetFormatter.out(System.out, rs);
//            System.out.println(results.size());
        queryExecution.close();
        return results;
    }
}
