package frontend.controllers;

import configuration.HeadersData;
import dictionary.IDictionary;
import dictionary.explanatory.ExplanatoryDictionary;
import dictionary.grammatical.GrammaticalDictionary;
import enums.DictionaryType;
import enums.WordOrderType;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import sparql.QueryData;
import sparql.QueryParser;
import sparql.SparqlService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainViewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView<IDictionary> listDictionary;

    @FXML
    private WebView webWordTemplate;

    @FXML
    private TextField tfWord;

    @FXML
    private TextField tfSearch;

    @FXML
    private Button btnSearch;

    @FXML
    private Button btnCreateDictionary;

    @FXML
    private ComboBox<String> cbDictionaryType;

    @FXML
    private TextArea taLog;

    private Stage primaryStage;
    private HeadersData dictionaryHeaders;
    private QueryParser queryParser;
    private static String forLog = "";
    private static boolean isForOne = false;

    public MainViewController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.dictionaryHeaders = new HeadersData();
        try {
            this.queryParser = new QueryParser();
        } catch (IOException e) {
            MainViewController.forLog += e.getMessage() + "\n";
        }
    }

    public static void addToForLog(String forLog) {
        MainViewController.forLog += forLog;
    }

    @FXML
    void btnCreateDictionaryOnAction(ActionEvent event) {
        forLog += "Запрос выполняется...\n";
        taLog.setText(forLog);
        DictionaryType dictionaryType = HeadersData.getDictionaryType(
                cbDictionaryType.getSelectionModel().getSelectedItem());

        if (tfWord.getText().compareTo("") == 0) {
            String query = QueryData.getQueryForAll(dictionaryType);
            listDictionary.getItems().clear();
            List<IDictionary> data = SparqlService.getResponseForAll(dictionaryType, WordOrderType.Lexicografical, query);

            if (data == null) {
                taLog.setText(forLog);
                return;
            }
            if (data.size() == 0) {
                forLog += String.format("По запросу \"%s словарь\" ничего не найдено\n",
                        cbDictionaryType.getSelectionModel().getSelectedItem().toLowerCase());
            } else {
                forLog += String.format("Был запрошен %s словарь (количество слов - %d)\n",
                        cbDictionaryType.getSelectionModel().getSelectedItem().toLowerCase(), data.size());
                listDictionary.getItems().addAll(data);
                isForOne = false;
            }
        } else {
            String query = QueryData.getQueryForOne(dictionaryType, tfWord.getText());
            listDictionary.getItems().clear();
            IDictionary data = SparqlService.getResponseForOne(
                    dictionaryType == DictionaryType.Grammatical ?
                            new GrammaticalDictionary(tfWord.getText()) :
                            new ExplanatoryDictionary(tfWord.getText()), query);
            if (data == null) {
                taLog.setText(forLog);
                return;
            }
            forLog += String.format("Был запрошен %s словарь, состоящий из слова \"%s\"\n",
                    cbDictionaryType.getSelectionModel().getSelectedItem().toLowerCase(), tfWord.getText());
            listDictionary.getItems().add(data);
            isForOne = true;
        }
        listDictionary.getSelectionModel().select(0);
        taLog.setText(forLog);
        tfWord.setText("");
    }

    @FXML
    void btnSearchOnAction(ActionEvent event) {
        if (tfSearch.getText().compareTo("") != 0 && listDictionary.getItems().size() > 1) {
            Optional<IDictionary> tmp = listDictionary.getItems().stream()
                    .filter(word -> word.getLemma().compareTo(tfSearch.getText()) == 0).findAny();
            if (tmp.isPresent()) {
                Platform.runLater(() -> scrollToSelectedItem(tmp.get()));
            } else {
                forLog += String.format("В полученном словаре нет слова \"%s\"", tfSearch.getText());
            }
            tfSearch.setText("");
        }
    }

    @FXML
    void initialize() {
        assert listDictionary != null : "fx:id=\"dictionary\" was not injected: check your FXML file 'mainView.fxml'.";
        assert webWordTemplate != null : "fx:id=\"taWordTemplate\" was not injected: check your FXML file 'mainView.fxml'.";
        assert tfSearch != null : "fx:id=\"tfWord\" was not injected: check your FXML file 'mainView.fxml'.";
        assert tfWord != null : "fx:id=\"tfWord\" was not injected: check your FXML file 'mainView.fxml'.";
        assert btnCreateDictionary != null : "fx:id=\"btnCreateDictionary\" was not injected: check your FXML file 'mainView.fxml'.";
        assert cbDictionaryType != null : "fx:id=\"cbDictionaryType\" was not injected: check your FXML file 'mainView.fxml'.";
        assert taLog != null : "fx:id=\"taLog\" was not injected: check your FXML file 'mainView.fxml'.";

        setComboBox();
        WebEngine webEngine = webWordTemplate.getEngine();

        listDictionary.setCellFactory(lv -> factoryForList());

        listDictionary.getItems().addListener((InvalidationListener) observable -> {
            if (listDictionary.getItems().isEmpty()) {
                webEngine.loadContent("");
            }
        });

        listDictionary.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null) {
                webEngine.loadContent("");
            } else {
                IDictionary tmp = listDictionary.getSelectionModel().getSelectedItem();
                System.out.println(tmp.getDictionaryType());
                switch (tmp.getDictionaryType()) {
                    case Grammatical: {
                        if (((GrammaticalDictionary) tmp).getWordForms().size() > 0) {
                            webEngine.loadContent(tmp.getDictionaryEntry());
                            return;
                        }
                        break;
                    }
                    case Explanatory: {
                        if (((ExplanatoryDictionary) tmp).getSenses().size() > 0) {
                            webEngine.loadContent(tmp.getDictionaryEntry());
                            return;
                        }
                        break;
                    }
                }
                if (isForOne) {
                    webEngine.loadContent(tmp.getDictionaryEntry());
                    return;
                }
                String query = QueryData.getQueryForOne(tmp.getDictionaryType(),
                        tfWord.getText().compareTo("") == 0 ? tmp.getLemma() : tfWord.getText()); //,
                IDictionary data = SparqlService.getResponseForOne(tmp, query);
                if (data != null)
                    webEngine.loadContent(data.getDictionaryEntry());
            }
        });

    }

    private void setComboBox() {
        cbDictionaryType.getItems().addAll(dictionaryHeaders.getTypesName());
        cbDictionaryType.getSelectionModel().select(0);
    }

    private ListCell<IDictionary> factoryForList() {
        return new ListCell<IDictionary>() {
            @Override
            protected void updateItem(IDictionary item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText("");
                } else {
                    setText(item.getLemma());
                }
            }
        };
    }

    private void scrollToSelectedItem(IDictionary tmp) {
        int idx = listDictionary.getItems().indexOf(tmp);
        listDictionary.scrollTo(idx);
        listDictionary.getSelectionModel().select(idx);
    }

}
