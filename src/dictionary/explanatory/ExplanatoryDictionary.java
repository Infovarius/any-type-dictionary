package dictionary.explanatory;

import dictionary.IDictionary;
import enums.DictionaryType;
import templates.explanatory.ExplanatoryTemplate;

import java.util.ArrayList;
import java.util.List;

public class ExplanatoryDictionary implements IDictionary {
    private String lemma;
    private List<String> senses;

    public ExplanatoryDictionary(String lemma) {
        this.lemma = lemma;
        this.senses = new ArrayList<>();
    }

    private ExplanatoryDictionary(String lemma, List<String> senses) {
        this.lemma = lemma;
        this.senses = senses;
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public DictionaryType getDictionaryType() {
        return DictionaryType.Explanatory;
    }

    @Override
    public String getDictionaryEntry() {
        return ExplanatoryTemplate.getDictionaryEntry(new ExplanatoryDictionary(lemma, senses));
    }

    public List<String> getSenses() {
        return senses;
    }

    public void setSenses(List<String> senses) {
        this.senses = senses;
    }
}
