package dictionary;

import enums.DictionaryType;

public interface IDictionary {

    String getLemma();

    void setLemma(String lemma);

    DictionaryType getDictionaryType();

    String getDictionaryEntry();

}
