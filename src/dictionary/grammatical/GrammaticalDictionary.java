package dictionary.grammatical;

import dictionary.IDictionary;
import enums.DictionaryType;
import enums.PartOfSpeech;
import org.javatuples.Pair;
import templates.grammatical.GrammaticalTemplate;

import java.util.ArrayList;
import java.util.List;

public class GrammaticalDictionary implements IDictionary {
    private String lemma;
    private Pair<PartOfSpeech, String> part;
    private String gender;
    private List<WordForms> wordForms;

    public GrammaticalDictionary(String lemma) {
        this.lemma = lemma;
        this.part = null;
        this.gender = "";
        this.wordForms = new ArrayList<>();
    }

    public GrammaticalDictionary(String lemma, Pair<PartOfSpeech, String> part, String gender, List<WordForms> wordForms) {
        this.lemma = lemma;
        this.part = part;
        this.gender = gender;
        this.wordForms = wordForms;
    }

    @Override
    public String getLemma() {
        return lemma;
    }

    @Override
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public DictionaryType getDictionaryType() {
        return DictionaryType.Grammatical;
    }

    @Override
    public String getDictionaryEntry() {
        return GrammaticalTemplate.getDictionaryEntry(
                new GrammaticalDictionary(lemma, part, gender, wordForms));
    }

    public Pair<PartOfSpeech, String> getPart() {
        return part;
    }

    public void setPart(Pair<PartOfSpeech, String> part) {
        this.part = part;
    }

    public String getGender() {
        return gender;
    }

    public List<WordForms> getWordForms() {
        return wordForms;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWordForms(List<WordForms> wordForms) {
        this.wordForms = wordForms;
    }
}
