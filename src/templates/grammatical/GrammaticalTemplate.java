package templates.grammatical;

import dictionary.grammatical.GrammaticalDictionary;
import templates.grammatical.partsOfSpeech.Adjective;
import templates.grammatical.partsOfSpeech.Adverb;
import templates.grammatical.partsOfSpeech.Noun;
import templates.grammatical.partsOfSpeech.Verb;


public class GrammaticalTemplate {

    public static String getDictionaryEntry(GrammaticalDictionary data) {
        StringBuilder htmlPage = new StringBuilder(String.format("<html lang=\"en\">\n" +
                        "<h4>Морфологические и синтаксические свойства</h4>\n" +
                        "<p>%s<br></p>\n" +
                        "<p>Часть речи: %s<br></p>\n",
                data.getLemma(), data.getPart().getValue1()));
        switch (data.getPart().getValue0()) {
            case Noun: {
                htmlPage.append(String.format("<p>Род: %s<br></p>\n", data.getGender()));
                htmlPage.append(Noun.setNounTable(data.getWordForms()));
                break;
            }
            case Verb: {
                htmlPage.append(Verb.setVerbTable(data.getWordForms()));
                break;
            }
            case Adjective: {
                htmlPage.append(Adjective.setAdjectiveTable(data.getWordForms()));
                break;
            }
            case Adverb: {
                htmlPage.append(Adverb.setAdverbTable(data.getWordForms()));
                break;
            }
            case None: {
                htmlPage.delete(0, htmlPage.length() - 1);
                htmlPage.append("<html lang=\"en\">");
                break;
            }
        }
        return htmlPage.append("</html>").toString();
    }

}
