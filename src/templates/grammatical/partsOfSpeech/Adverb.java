package templates.grammatical.partsOfSpeech;

import dictionary.grammatical.WordForms;

import java.util.List;

public class Adverb {

    public static String setAdverbTable(List<WordForms> wordForms) {
        StringBuilder htmlPage = new StringBuilder("<table border=\"1\" cellspacing=\"0\"\n" +
                "  cellpadding=\"10\">\n" +
                "  <tbody>\n");
        for (WordForms word : wordForms) {
            String table = String.format("<tr>\n" +
                    "  <td bgcolor=\"#EEF9FF\">%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "</tr>\n", word.getLemmas().get(0), word.getWord());
            htmlPage.append(table);
        }
        htmlPage.append("  </tbody></table>\n");
        return htmlPage.toString();
    }

}
