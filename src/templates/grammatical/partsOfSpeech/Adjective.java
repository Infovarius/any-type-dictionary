package templates.grammatical.partsOfSpeech;

import dictionary.grammatical.WordForms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Adjective {

    private static String setSingularsOrPluralsForAdjectives(String gender, List<WordForms> caseWordForms) {
        return caseWordForms.stream().filter(word -> word.getLemmas().contains(gender)
                && word.getLemmas().contains("единственное число"))
                .map(WordForms::getWord).collect(Collectors.joining(",\n"));
    }

    private static String setSingularsOrPluralsForAdjectivesForAnimation(boolean isSingular, List<WordForms> caseWordForms) {
        if (isSingular) {
            String singleMale = caseWordForms.stream().filter(word -> word.getLemmas().contains("мужской род")
                    && word.getLemmas().contains("единственное число")
                    && word.getLemmas().contains("одушевлённое"))
                    .map(WordForms::getWord).collect(Collectors.joining(",\n"));
            singleMale += "=";
            singleMale += caseWordForms.stream().filter(word -> word.getLemmas().contains("мужской род")
                    && word.getLemmas().contains("единственное число")
                    && word.getLemmas().contains("неодушевлённое"))
                    .map(WordForms::getWord).collect(Collectors.joining(",\n"));
            return singleMale;
        } else {
            String plural = caseWordForms.stream().filter(word -> word.getLemmas().contains("множественное число")
                    && word.getLemmas().contains("одушевлённое"))
                    .map(WordForms::getWord).collect(Collectors.joining(",\n"));
            plural += "=";
            plural += caseWordForms.stream().filter(word -> word.getLemmas().contains("множественное число")
                    && word.getLemmas().contains("неодушевлённое"))
                    .map(WordForms::getWord).collect(Collectors.joining(",\n"));
            return plural;
        }
    }

    private static List<List<String>> getCasesForAdjectives(List<WordForms> wordForms) {
        List<List<String>> cases = new ArrayList<>();
        List<String> allCasesTmp = new ArrayList<>(Noun.getAllCases());
        allCasesTmp.add("краткая форма прилагательных");
        for (String caseName : allCasesTmp) {
            List<WordForms> caseWordForms = Noun.getCase(wordForms, caseName);
            if (caseWordForms.size() > 0)
                if (caseName.compareTo("винительный падеж") == 0) {
                    cases.add(Arrays.asList(caseName,
                            setSingularsOrPluralsForAdjectivesForAnimation(true, caseWordForms),
                            setSingularsOrPluralsForAdjectives("средний род", caseWordForms),
                            setSingularsOrPluralsForAdjectives("женский род", caseWordForms),
                            setSingularsOrPluralsForAdjectivesForAnimation(false, caseWordForms)));
                } else {
                    cases.add(Arrays.asList(caseName,
                            setSingularsOrPluralsForAdjectives("мужской род", caseWordForms),
                            setSingularsOrPluralsForAdjectives("средний род", caseWordForms),
                            setSingularsOrPluralsForAdjectives("женский род", caseWordForms),
                            Noun.setSingularsOrPluralsForNouns(false, caseWordForms)));
                }
        }
        return cases;
    }

    public static String setAdjectiveTable(List<WordForms> wordForms) {
        StringBuilder htmlPage = new StringBuilder("<table border=\"1\" cellspacing=\"0\"\n" +
                "  cellpadding=\"10\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th bgcolor=\"#EEF9FF\" colspan=\"2\" rowspan=\"2\"></th>\n" +
                "      <th bgcolor=\"#EEF9FF\" colspan=\"3\">единственное число</th>\n" +
                "      <th bgcolor=\"#EEF9FF\" rowspan=\"2\">множественное число</th>\n" +
                "    </tr>\n" +
                "    <tr bgcolor=\"#EEF9FF\">\n" +
                "      <th>мужской род</th>\n" +
                "      <th>средний род</th>\n" +
                "      <th>женский род</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n");
        for (List<String> caseData : getCasesForAdjectives(wordForms)) {
            String table = caseData.get(0).compareTo("винительный падеж") != 0
                    ? String.format("<tr>\n" +
                    "  <td bgcolor=\"#EEF9FF\" colspan=\"2\" >%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "  <td>%s</td>\n" +
                    "</tr>\n", caseData.get(0), caseData.get(1), caseData.get(2), caseData.get(3), caseData.get(4))
                    : String.format("<tr>\n" +
                            "  <td rowspan=\"2\" bgcolor=\"#EEF9FF\">%s</td>\n" +
                            "  <td bgcolor=\"#EEF9FF\">одушевлённый</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td rowspan=\"2\">%s</td>" +
                            "  <td rowspan=\"2\">%s</td>" +
                            "  <td>%s</td>\n" +
                            "</tr>\n" +
                            "<tr>\n" +
                            "  <td bgcolor=\"#EEF9FF\">неодушевлённый</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "</tr>\n", caseData.get(0), caseData.get(1).split("=")[0], caseData.get(2), caseData.get(3),
                    caseData.get(4).split("=")[0], caseData.get(1).split("=")[1], caseData.get(4).split("=")[1]);
            htmlPage.append(table);
        }
        htmlPage.append("  </tbody></table>\n");
        return htmlPage.toString();
    }

}
