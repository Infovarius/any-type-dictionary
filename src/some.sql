SELECT ?l ?lemma ?partLabel ?genderLabel ?word (GROUP_CONCAT(DISTINCT ?grammaticalFeature; SEPARATOR=', ') AS ?lem) WHERE {
    ?l a ontolex:LexicalEntry ; dct:language ?language ;
#         wikibase:lexicalCategory ?part ; #wd:Q34698 ; wd:Q24905 ; wd:Q1084 ; wd:Q380057 ;
        wikibase:lexicalCategory wd:Q34698 ;
        wikibase:lemma ?lemma ;
        ontolex:lexicalForm ?form .
    ?form ontolex:representation ?word ;
        wikibase:grammaticalFeature ?grammaticalFeature .
    optional { ?l wdt:P5185 ?gender. }
    ?language wdt:P218 'ru'.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
} GROUP BY ?l ?lemma ?partLabel ?genderLabel ?word ?lem
LIMIT 1