package configuration;

public class PathConfig {
    private static final String icoMainPath = "/images/dictionary.png";
    private static final String entitiesNamesPath = "resources/entities_names/entities_names.txt";
    private static final String mainViewPath = "/frontend/views/mainView.fxml";

    public static String getIcoMainPath() {
        return icoMainPath;
    }

    public static String getEntitiesNamesPath() {
        return entitiesNamesPath;
    }

    public static String getMainViewPath() {
        return mainViewPath;
    }

}
